<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class CartController extends Controller
{
    public function add(Request $request)
    {
        $item = Cart::query()->create([
            'user_id' => Auth::id(),
            'item' => $request['item']
        ]);

    }

    public function my(Request $request)
    {
        return Cart::query()->where('user_id', Auth::id())->orderBy('id', 'DESC')->get();
    }

    public function buy(Request $request)
    {
        $item = $request['item'];
        $request = Http::get('https://testepg.guavapay.com/epg/rest/register.do?userName='.env('GUAVA_LOGIN').'&password='.env('GUAVA_PASS').'&currency=978&orderNumber='.rand(0,99999).'&amount='.($request['price'] * 100).'&language=en&returnUrl=/payment/cart.html&jsonParams={"request":"PAY","bank":"000","description":"PAY","sid":"900000"}');
        $response = $request->body();

        $link = json_decode($response)->formUrl;

        Order::query()->create([
            'user_id' => Auth::id(),
            'item' => $item,
            'payment_link' => $link
        ]);

        return $link;
    }

    public function myOrders()
    {
        return Order::query()->where('user_id', Auth::id())->orderBy('id', 'DESC')->get();
    }

}
