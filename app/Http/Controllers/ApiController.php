<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ApiController extends Controller
{
    public string $key;
    public function __construct()
    {
            $a = Http::withHeaders([
                'Accept-Encoding' => 'gzip'
            ])->post('https://p22-ma-ak.crpo.su/api/user/login', [
                'login' => 'api_fa.kodzoeva@gmail.com',
                'password' => 'cGvC1c_0AA'
            ])->json();

            $this->key = $a['data']['auth_token'];

            $path = storage_path('key/').'app.key';
            file_put_contents($path, $a['data']['auth_token']);
            chmod($path, 0777);
            $key = file_get_contents(storage_path('key/').'app.key');

    }

    public function completeCity(Request $request)
    {
        $data = Http::withHeaders([
            'Accept-Encoding' => 'gzip'
        ])->post('https://p22-ma-ak.crpo.su/api/avia/airports?part='.$request['req'].'&lang=en&auth_key='.$this->key)->json();

        $resp = [];

        foreach ($data['data']['cities'] as $key => $value) {
            if(isset($value['airports'] )) {
                foreach ($value['airports'] as $airports) {
                    $item = [
                        'country' => $value['countryName'],
                        'cityName' => $value['cityName'],
                        'airportName' => $airports['airportName'],
                        'airportIataCode' => $airports['airportIataCode'],
                    ];

                    $resp[] = $item;
                }
            } else {
                $item = [
                    'country' => $value['countryName'],
                    'cityName' => $value['cityName'],
                    'airportName' => $value['cityIataCode'],
                    'airportIataCode' => $value['cityIataCode'],
                ];

                $resp[] = $item;
            }

        }

        $final = [];
        foreach ($resp as $item) {
            $final[] = "{$item['airportIataCode']}: {$item['country']} - {$item['cityName']} - {$item['airportName']}";
        }
        return $final;
    }

    public function completeHotel(Request $request)
    {
        $data = Http::withHeaders([
            'Accept-Encoding' => 'gzip'
        ])->get('https://p22-ma-ak.crpo.su/api/hotel/search/destination?part='.$request['req'].'&lang=ru&auth_key='.$this->key)->json();

        $resp = [];

        foreach ($data['data']['regions'] as $key => $value) {
            $re = [
                'id' => $value['iata'],
                'value' => $value['name']
            ];

            $resp[] = $value['iata'].':'.$value['name'];
        }

        return $resp;
    }

    public function getHotels(Request $request)
    {
        $city = $request['city'];
        $dateStart = $request['dateStart'];
        $dateEnd = $request['dateEnd'];
        $room = $request['room'];
        $adults = $request['adults'];
        $children = $request['children'];
        $stairs = $request['stairs'];

        $url = "https://p22-ma-ak.crpo.su/api/hotel/search?search[adults]=$adults&search[city]=$city&search[check_in]=$dateStart&search[check_out]=$dateEnd";

        for ($i = 0; $i < $children; $i++) {
            $url .= "&search[children][$i][child_age]=5";
        }

        $url .= "&lang=en&auth_key=$this->key";

        $resp = Http::withHeaders([
            'Accept-Encoding' => 'gzip'
        ])->get($url)->json();

        if (empty($resp['data']['hotels'])) {
            return !empty($resp['data']['message'])
                ? response()->json(['success' => false, 'message' => $resp['data']['message']], 401)
                : response()->json(['success' => false, 'message' => "Nothing found"], 404);
        }

        if (!empty($stairs)) {
            return array_filter($resp['data']['hotels'], function ($hotel) use ($stairs) {
                return $hotel['stars'] == $stairs;
            });
        }

        return $resp['data']['hotels'];
    }

    public function getFlights(Request $request)
    {
        $airportFrom = $request['airportFrom'];
        $airportTo   = $request['airportTo'];
        $dateStart   = $request['dateStart'];
        $dateEnd     = $request['dateEnd'];

        $adults   = $request['adults'];
        $children = $request['children'];
        $infants  = $request['infants'];

        $flightClass = match($request['flightClass']) {
            'Economy' => 'e',
            'First class' => 'f',
            'Business class' => 'b',
             default => 'a',
        };

        $isRoundtrip = $request['isRoundtrip'];

        if (!$isRoundtrip)
            $url = "https://p22-ma-ak.crpo.su/api/avia/search-recommendations?adt=$adults&chd=$children&inf=$infants&src=0&yth=0&class=$flightClass&segments[0][from]=$airportFrom&segments[0][to]=$airportTo&segments[0][date]=$dateStart&is_direct_only=1&lang=en&auth_key=$this->key";
        else
            $url = "https://p22-ma-ak.crpo.su/api/avia/search-recommendations?adt=$adults&chd=$children&inf=$infants&src=0&yth=0&class=$flightClass&segments[0][from]=$airportFrom&segments[0][to]=$airportTo&segments[0][date]=$dateStart&segments[1][from]=$airportTo&segments[1][to]=$airportFrom&segments[1][date]=$dateEnd&is_direct_only=1&lang=en&auth_key=$this->key";

        $resp = Http::withHeaders([
            'Accept-Encoding' => 'gzip'
        ])->post($url)->json();

        if (empty($resp['data']['flights'])) {
            return !empty($resp['data']['message'])
                ? response()->json(['success' => false, 'message' => $resp['data']['message']], 401)
                : response()->json(['success' => false, 'message' => "Nothing found"], 404);
        }

        $results = $resp['data']['flights'];

        $finalResponse = [];
        foreach ($results as $result) {
            $item = [
                'id' => $result['id'],
                'price' => $result['price']['RUB']['amount'],
                'arriveDate' => $result['segments'][0]['arr']['date'],
                'arriveTime' => $result['segments'][0]['arr']['time'],
                'arriveAirport' => $result['segments'][0]['arr']['airport'],
                'arriveCity' => $result['segments'][0]['arr']['city'],
                'depDate' => $result['segments'][0]['dep']['date'],
                'depTime' => $result['segments'][0]['dep']['time'],
                'depAirport' => $result['segments'][0]['dep']['airport'],
                'depCity' => $result['segments'][0]['dep']['city'],
                'duration' => $result['segments'][0]['duration'],
                'aircraft' => $result['segments'][0]['aircraft'],
                'provider' => $result['provider'],
            ];

            $finalResponse[] = $item;
        }

        return $finalResponse;
    }
}
