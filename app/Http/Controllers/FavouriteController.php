<?php

namespace App\Http\Controllers;

use App\Models\Favourite;
use Illuminate\Http\Request;

class FavouriteController extends Controller
{
    public function get(Request $request)
    {
        $authID = $request->query('userId');

        $data = [];
        if ($authID) {
            $query = Favourite::query()->where('user_id', $authID);

            if ($request->has('tab')) {
                $tabs = [
                    'all' => null,
                    'hotels'  => 'hotel',
                    'flights' => 'flight'
                ];

                if (isset($tabs[$request->query('tab')]))
                    $query ->where('tab', $tabs[$request->query('tab')]);
            }

            $data = $query->get();
        }

        return response()->json(['data' => $data]);
    }

    public function create(Request $request)
    {
        $authID = $request->input('userId');

        $data = false;
        if ($authID) {
            $data = Favourite::query()->firstOrCreate([
                'user_id' => $authID,
                'tab' => $request->input('tab'),
                'id_favourite' => $request->input('id_favourite')
            ], [
                'user_id' => $authID,
                'tab' => $request->input('tab'),
                'id_favourite' => $request->input('id_favourite'),
                'item' => json_encode($request->input('item'))
            ]);
        }

        return response()->json(['success' => !!$data]);
    }

    public function remove(Request $request)
    {
        $authID = $request->input('userId');

        $data = false;
        if ($authID) {
            $data = Favourite::query()
                ->where('user_id', $authID)
                ->where('tab', $request->input('tab'))
                ->where('id_favourite', $request->input('id_favourite'))
                ->delete();
        }

        return response()->json(['success' => !!$data]);
    }
}
