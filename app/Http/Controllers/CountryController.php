<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function get()
    {
        return Country::query()->get();
    }

    public function getForMainPage()
    {
        return Country::query()->latest()->take(6)->get();
    }

    public function getItem(Request $request)
    {
        return Country::query()->where(['name' => $request->input('name')])->first();
    }
}
