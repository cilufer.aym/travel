<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class AuthController extends Controller
{
    public function register(RegisterRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();

            $user = User::query()->create([
                'name'  => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);

            if ($user) {
                DB::commit();

                $success['token'] = $user->createToken('MyApp')->accessToken;
                $success['name']  = $user->name;
                return response()->json(['success' => $success, 'user' => $user]);
            }

            throw new \Exception("Can't create your profile", 401);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 401);
        }
    }

    public function login(LoginRequest $request): JsonResponse
    {
        try {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $user = Auth::user();
                $success['token'] = $user->createToken('MyApp')->accessToken;
                return response()->json(['success' => $success, 'user' => $user]);
            } else {
                return response()->json(['success' => false, 'message' => 'Wrong login or password'], 401);
            }
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 401);
        }
    }

    public function editProfile(Request $request)
    {
        $request->validate([
            'name'     => ['required', 'string', 'max:255'],
            'email'    => ['required', 'email', 'regex:/^[\w\.~-]+@([a-zA-Z-]+\.)+[a-zA-Z-]{2,4}$/i',
                Rule::unique('users')->ignore($request->id), 'string', 'max:255'],
            'phone' => ['required', Rule::unique('users')->ignore($request->id)]
        ]);

        try {
            DB::beginTransaction();

            $user = User::query()->where(['id' => $request->id])->update([
                'name'  => $request->name,
                'phone' => $request->phone,
                'email' => $request->email
            ]);

            if ($user) {
                DB::commit();
                return response()->json(['success' => !!$user, 'user' => $user]);
            }

            throw new \Exception("Can't update your profile", 401);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 401);
        }
    }
}
