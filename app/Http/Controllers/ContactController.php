<?php

namespace App\Http\Controllers;

use App\Models\ContactMessages;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function sendQuestion(Request $request): JsonResponse
    {
        $data = $request->validate([
            'phone' => ['required', 'regex:/^[0-9()+\-\s]*$/'],
            'email' => ['required', 'email', 'regex:/^[\w\.~-]+@([a-zA-Z-]+\.)+[a-zA-Z-]{2,4}$/i',
                'string', 'max:255'],
            'question' => ['required', 'string'],
        ]);

        $isSend = ContactMessages::query()->create([
            'email' => $data['email'],
            'phone' => $data['phone'],
            'question' => $data['question'],
        ]);

        return response()->json(['success' => !!$isSend]);
    }
}
