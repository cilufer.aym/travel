<?php

namespace App\Http\Controllers;

use App\Models\information;
use Illuminate\Http\Request;

class InformationController extends Controller
{
    public function get()
    {
        return information::query()->first();
    }
}
