<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Pusher\Pusher;

class ChatController extends Controller
{
    public function send(Request $request)
    {
        if($request->hasFile('image')){
            // $path = Storage::disk('local')->put($request->file('photo')->getClientOriginalName(),$request->file('photo')->get());
            $path = $request->file('image')->store('/public/images/1/smalls');
            $path = str_replace('public', '', $path);

            $text = '<img width="150px" src="/storage/'.$path.'">';
        } else {
            $text = $request['message'];
        }
        $chat = Chat::query()->where('owner_id', Auth::id())->first();

        if(!$chat) {
            $chat = Chat::query()->create([
                'owner_id' => Auth::id(),
            ]);
        }

        $message = Message::query()->create([
            'chat_id' => $chat->id,
            'user_id' => Auth::id(),
            'text' => $text
        ]);

        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            array(
                'cluster' => 'eu',
                'useTLS' => true
            )
        );

        $pusher->trigger('chat_'.$chat->owner_id, 'new-message', $message->toArray());
    }

    public function get(Request $request)
    {
        $chat = Chat::query()->where('owner_id', Auth::id())->first();

        if(!$chat) {
            $chat = Chat::query()->create([
                'owner_id' => Auth::id(),
            ]);
        }
        
        return Chat::query()->with('messages.user')->where('owner_id', Auth::id())->first();
    }
}
