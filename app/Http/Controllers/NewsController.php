<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function get()
    {
        return News::query()->get();
    }

    public function getForMainPage()
    {
        return News::query()->latest()->take(4)->get();
    }

    public function getItem(Request $request)
    {
        return News::query()->where(['stub' => $request->input('name')])->first();
    }
}
