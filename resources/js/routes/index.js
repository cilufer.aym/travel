import Home from '../components/Home.vue';
import About from '../components/About.vue';
import FlightResults from "../components/FlightResults.vue";
import HotelsResults from "../components/HotelsResults.vue";
import ShoppingCart from "../components/ShoppingCart.vue";
import Chat from "../components/Chat.vue";
import Contact from "../components/Contact.vue";
import Faq from "../components/Faq.vue";
import Help from "../components/Help.vue";
import HotelCard from "../components/HotelCard.vue";
import Blog from "../components/Blog.vue";
import BlogCard from "../components/BlogCard.vue";
import Reviews from "../components/Reviews.vue";
import PrivacyPolicy from "../components/PrivacyPolicy.vue";
import TermsAndConditions from "../components/TermsAndConditions.vue";
import CancellationAndRefund from "../components/CancellationAndRefund.vue";
import ProfileLayout from "../components/ProfileLayout.vue";

export const routes = [
    {
        path: '/',
        component: Home,
        name: 'Home'
    },
    {
        path: '/profile/:tab/:type?',
        component: ProfileLayout,
        name: 'Profile',
        meta: { requiresAuth: true }
    },
    {
        path: '/cart',
        component: ShoppingCart,
        name: 'Cart',
        meta: { requiresAuth: true }
    },
    {
        path: '/flight/results/:from/:to/:dateStart/:dateEnd?/:adults/:children/:infants/:flightClass/:isRoundtrip',
        component: FlightResults,
        props: true,
        name: 'ResultFlight'
    },
    {
        path: '/hotel/results/:city/:dateStart/:dateEnd/:room/:adults/:children/:stairs',
        component: HotelsResults,
        props: true,
        name: 'ResultHotel'
    },
    {
        path: '/blog',
        component: Blog,
        name: 'Blog'
    },
    {
        path: '/country/:name',
        props: true,
        component: BlogCard,
        name: 'CountryCard'
    },
    {
        path: '/news/:stub',
        props: true,
        component: BlogCard,
        name: 'NewsCard'
    },
    {
        path: '/chat',
        component: Chat,
        name: 'Chatting',
        meta: { requiresAuth: true }
    },
    {
        path: '/hotel/card/:name',
        component: HotelCard,
        name: 'HotelCard'
    },
    {
        path: '/reviews',
        component: Reviews,
        name: 'Reviews'
    },
    {
        path: '/about',
        component: About,
        name: 'About'
    },
    {
        path: '/help',
        component: Help,
        name: 'Help'
    },
    {
        path: '/contact',
        component: Contact,
        name: 'Contact'
    },
    {
        path: '/faq',
        component: Faq,
        name: 'Faq'
    },
    {
        path: '/privacy-policy',
        component: PrivacyPolicy,
        name: 'PrivacyPolicy'
    },
    {
        path: '/cancellation-and-refund',
        component: CancellationAndRefund,
        name: 'CancellationAndRefund'
    },
    {
        path: '/terms-and-conditions',
        component: TermsAndConditions,
        name: 'TermsAndConditions'
    },
];
