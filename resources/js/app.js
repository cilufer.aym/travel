import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router';
import PrimeVue from 'primevue/config';
import App from './components/App.vue';
import { routes } from './routes';
import Button from "primevue/button"
import AutoComplete from 'primevue/autocomplete';
import 'primevue/resources/themes/aura-light-green/theme.css'

const router = new createRouter({
    history: createWebHistory(),
    routes
});

router.beforeEach((to, from, next) => {
    if (!!to.meta.requiresAuth) {

        if (!localStorage || !localStorage.user) {
            const popupButton = document.querySelector('.show_popup');
            if (popupButton) {
                popupButton.click();
            }

        } else {
            next();
        }
    } else {
        next();
    }
});

const app = createApp(App)
    app.use(router)
    app.use(PrimeVue)
    app.component('Button', Button);
    app.component('AutoComplete', AutoComplete);
    app.mount("#app")
