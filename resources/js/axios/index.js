import axios from 'axios'

export const updateAuthToken = () => {
    if (localStorage.getItem('access_token')) {
        axios.defaults.headers.common['Authorization'] =
            'Bearer ' + localStorage.getItem('access_token')
    }
}

updateAuthToken()

export default axios
