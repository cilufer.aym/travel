<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [\App\Http\Controllers\AuthController::class, 'login']);
    Route::post('register', [\App\Http\Controllers\AuthController::class, 'register']);
    Route::post('edit-profile', [\App\Http\Controllers\AuthController::class, 'editProfile']);
});
Route::group(['prefix' => 'crpo'], function () {
    Route::post('complete', [\App\Http\Controllers\ApiController::class, 'completeCity']);
    Route::post('completeHotel', [\App\Http\Controllers\ApiController::class, 'completeHotel']);
    Route::post('getFlights', [\App\Http\Controllers\ApiController::class, 'getFlights']);
    Route::post('getHotels', [\App\Http\Controllers\ApiController::class, 'getHotels']);
});
Route::group(['prefix' => 'contact'], function () {
    Route::post('send-question', [\App\Http\Controllers\ContactController::class, 'sendQuestion']);
});
Route::group(['prefix' => 'country'], function () {
    Route::get('get', [\App\Http\Controllers\CountryController::class, 'get']);
    Route::get('get-for-main-page', [\App\Http\Controllers\CountryController::class, 'getForMainPage']);
    Route::get('get-item', [\App\Http\Controllers\CountryController::class, 'getItem']);
});
Route::group(['prefix' => 'news'], function () {
    Route::get('get', [\App\Http\Controllers\NewsController::class, 'get']);
    Route::get('get-for-main-page', [\App\Http\Controllers\NewsController::class, 'getForMainPage']);
    Route::get('get-item', [\App\Http\Controllers\NewsController::class, 'getItem']);
});
Route::group(['prefix' => 'questions'], function () {
    Route::get('get', [\App\Http\Controllers\QuestionController::class, 'get']);
});

Route::group(['prefix' => 'information'], function () {
    Route::get('get', [\App\Http\Controllers\InformationController::class, 'get']);
});

Route::group(['prefix' => 'favourite'], function () {
    Route::get('get', [\App\Http\Controllers\FavouriteController::class, 'get']);
    Route::post('create', [\App\Http\Controllers\FavouriteController::class, 'create']);
    Route::delete('remove', [\App\Http\Controllers\FavouriteController::class, 'remove']);
});

Route::group(['middleware' => 'auth:api'], function() {
    Route::group(['prefix' => 'cart'], function () {
        Route::post('add', [\App\Http\Controllers\CartController::class, 'add']);
        Route::get('my', [\App\Http\Controllers\CartController::class, 'my']);
    });
    Route::group(['prefix' => 'payment'], function () {
        Route::post('buy', [\App\Http\Controllers\CartController::class, 'buy']);
    });
    Route::group(['prefix' => 'orders'], function () {
        Route::get('my', [\App\Http\Controllers\CartController::class, 'myOrders']);
        Route::post('create', [\App\Http\Controllers\CartController::class, 'createOrder']);
    });
    Route::group(['prefix' => 'chat'], function () {
        Route::get('get_messages', [\App\Http\Controllers\ChatController::class, 'get']);
        Route::post('send_message', [\App\Http\Controllers\ChatController::class, 'send']);
    });
});
